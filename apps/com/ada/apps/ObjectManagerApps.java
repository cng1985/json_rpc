/** 
 * CopyRright (c)1985-2012:宝亿电子 <br />                             
 * Project: jsonrpc<br />                                           
 * Module ID:    <br />   
 * Comments:            <br />                                  
 * JDK version used:<JDK1.6><br />                                 
 * Namespace:package com.ada.apps;<br />                             
 * Author：陈联高 <br />                  
 * Create Date：  2012-10-12<br />   
 * Modified By：ada.young          <br />                                
 * Modified Date:2012-10-12      <br />                               
 * Why & What is modified <br />   
 * Version: 1.01         <br />       
 */

package com.ada.apps;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.json.rpc.commons.AllowAllTypeChecker;
import org.json.rpc.server.HandleEntry;
import org.json.rpc2.ObjectManager;
import org.json.rpc2.ObjectManagerBase;

public class ObjectManagerApps {

	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ObjectManager om = new ObjectManagerBase(new AllowAllTypeChecker());
		Work work = new WorkImpl();
		om.addHandler("work", work, Work.class);
		HandleEntry<?> works = om.get("work");
		Method[] ms = works.getClass().getMethods();
		for (int i = 0; i < ms.length; i++) {
			Method m = ms[i];
			System.out.println(m.getName());
		}
		System.out.println("###########");
		Object[] mss = works.getMethods().toArray();
		for (int i = 0; i < mss.length; i++) {
			Method m = (Method) mss[i];
			System.out.println(m.getName());
			try {
				Object a = m.invoke(works.getHandler(), 1, 1);
				System.out.println(a);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
